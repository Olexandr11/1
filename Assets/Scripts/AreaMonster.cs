using UnityEngine;

public class AreaMonster : MonoBehaviour
{
    [SerializeField] private float _radius;
    [SerializeField] private GameObject _monsterGameObject;

    private MonsterController _monster;

    void Start()
    {
        GameObject newMonster = Instantiate(_monsterGameObject, transform.position, Quaternion.identity);
        _monster = newMonster.GetComponent<MonsterController>();
        _monster.MoveDirection();
        _monster.SpawnObject(gameObject);

        gameObject.GetComponent<CircleCollider2D>().radius =_radius;
        gameObject.GetComponent<CircleCollider2D>().isTrigger = true;
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == _monster.gameObject)_monster.MoveDirection();   
    }
}
