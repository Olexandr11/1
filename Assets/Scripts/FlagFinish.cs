using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

[RequireComponent(typeof(BoxCollider2D))]
public class FlagFinish : InteractableObject
{
    [SerializeField] private Text _timer;
    [SerializeField] private Text _totalTime;
    [SerializeField] private Text _mistakes;
    [SerializeField] private Text _mushrooms;

    [SerializeField] private UnityEvent _finishEvent;
    private float _time;
    private bool _isFinished = false;
    
    void Start()
    {
        _time = 0f;

        _isFinished = false;
    }

    void Update()
    {
        if (_isFinished == true) return;  
        
        _time += Time.deltaTime;
        _timer.text = Math.Round(_time, 1).ToString(); 
    }

    public override void Interact(Player player)
    {
        _isFinished = true;

        if (PlayerPrefs.HasKey("Record") != true) 
            PlayerPrefs.SetFloat("Record", (float)Math.Round(_time, 1));

        if (Math.Round(_time, 1) < PlayerPrefs.GetFloat("Record")) PlayerPrefs.SetFloat("Record", (float)Math.Round(_time, 1));

        _totalTime.text = "Time: " + Math.Round(_time, 1).ToString() + "s";
        _mistakes.text = "Mistakes: " + player.Mistakes.ToString();
        _mushrooms.text = "Mushrooms: " + player.Mushrooms.ToString();
        _finishEvent.Invoke();
    }
}
