using UnityEngine;

public abstract class InteractableObject : MonoBehaviour
{
    abstract public void Interact(Player player);
}
