using UnityEngine;

public class Magicwand : Item
{
    [SerializeField] private Projectile _magicProjectile;
    [SerializeField] private Item.TypeUse _type;
    public override TypeUse Type => _type;

    public override void Interection(Player player)
    {
        Projectile newProjectile = Instantiate(_magicProjectile, player.Hand.GetChild(0).GetChild(0).position, Quaternion.identity);
        newProjectile.FlyBullet(player.Angle);

        newProjectile.HeadPlayer = player;
    }
}
