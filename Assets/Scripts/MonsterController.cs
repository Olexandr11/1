using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MonsterController : NonPlayerCharacter
{
    [SerializeField] private Text _damageText;
    [SerializeField] private UnityEvent _deth;

    private float time;
    
    private bool _isDamaged;

    private SpriteRenderer _meSprite;

    private GameObject _mainSpawner;
    
    private Walk _fox;
    private Player _mainPlayer;

    private Vector2 direction;
    private Vector2 spawnPosition;

    private int _hp = 10;

    public override void MoveDirection()
    {
        System.Random randomPoint = new System.Random();

        Vector2 vect = new Vector2(randomPoint.Next(-4, 5) * 0.1f + spawnPosition.x,
           randomPoint.Next(-4, 5) * 0.1f + spawnPosition.y);
        direction = (vect - (Vector2)transform.position).normalized;
    }

    void Start()
    {
        _meSprite = GetComponent<SpriteRenderer>();

        spawnPosition = transform.position;

        _fox = FindObjectOfType<Walk>(true);
    }


    void Update()
    {
        if (_isDamaged)
        {           
            time += Time.deltaTime;
            if (time >= 4f)
            {
                transform.localScale = new Vector3(0.5f,0.5f,0.5f);
                _meSprite.color = Color.blue;
                _hp = 10;
                
                _isDamaged = false;

                time = 0f;
            }
        }

        transform.Translate(direction*2f*Time.deltaTime);
    }

    public void SpawnObject(GameObject spawner)
    {
        _mainSpawner = spawner;
    }

    public override void Interection(Player player)
    {
        player.Mistakes++;
        _fox.SwapOnGhost();
    }

    private void OnDestroy()
    {
        if(_mainPlayer)_mainPlayer.AddDamage(3);

        Destroy(_mainSpawner);
    }

    public override void TakeDamage(int damage,Player player)
    {
        _mainPlayer = player;

        _hp -= damage;
        
        _deth.Invoke();

        transform.localScale = new Vector3(transform.localScale.x + damage * 0.05f, 
            transform.localScale.y + damage * 0.05f, transform.localScale.z + damage * 0.05f);

        _meSprite.color = Color.red;

        _isDamaged = true;

        _damageText.text ="-" + damage.ToString();
        _damageText.CrossFadeAlpha(1, 0.1f, false);
        _damageText.CrossFadeAlpha(0, 1f, false);

        if (_hp <= 0f) Destroy(gameObject);
    }
}
