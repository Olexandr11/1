using UnityEngine;

public class MushroomBaf : Item
{
    [SerializeField] private Item.TypeUse _type;

    public override TypeUse Type => _type;

    public override void Interection(Player player)
    {
        player.transform.Translate(Vector3.right * 2f);
    }
}
