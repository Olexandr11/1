using UnityEngine;
using UnityEngine.Events;

public class MushroomPoint : InteractableObject
{
    [SerializeField] private UnityEvent _interact;

    public override void Interact(Player player)
    {
        ++player.Mushrooms;
        player.ItemChanges();
        _interact.Invoke();
         Destroy(gameObject);
    }
}
