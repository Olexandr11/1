using UnityEngine;

public abstract class NonPlayerCharacter : MonoBehaviour
{
    public abstract void TakeDamage(int damage, Player player);
    public abstract void MoveDirection();
    public abstract void Interection(Player player);
}
