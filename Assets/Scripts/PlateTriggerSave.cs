using UnityEngine;
using UnityEngine.Events;

public class PlateTriggerSave : InteractableObject
{
    [SerializeField] private UnityEvent _intearect;

    public override void Interact(Player player)
    {
        player.GetComponent<SaveSerial>().SaveGame();
        _intearect.Invoke();
    }
}
