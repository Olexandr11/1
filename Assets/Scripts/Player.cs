using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    [SerializeField] private Text[] _textInterface;

    [SerializeField] private UnityEvent _eventDamage;
    [SerializeField] private UnityEvent _eventMushroom;
   
    public  Transform Hand;

    [HideInInspector] public int Damage, Mushrooms, Mistakes;
    [HideInInspector] public float Angle;

    [HideInInspector] public Item CurrentItem;
        
    private MonsterController monst;

    private void Start()
    {
        Hand = transform.GetChild(0);
        ItemChanges();
        Damage = 3;
        gameObject.GetComponent<SaveSerial>().SaveGame();   

        _textInterface[0].text = Damage.ToString();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.TryGetComponent(out NonPlayerCharacter monster)) 
        {
            monster.Interection(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out InteractableObject interacty)) 
        { 
            interacty.Interact(this);            
        }
    }

    public void Intepact(Item item)
    {
        if (item == null) return;

        item.Interection(this);
    }

    public void SwapIItem(IItem item)
    {

        for (int i = 0; i < Hand.childCount; i++)
        {
            Destroy(Hand.GetChild(i).gameObject);
        }

        if (item == null)
        {
            CurrentItem = null;
            return;
        }

        
        var gameObjectInHand = Instantiate(item.PrefabObject, Hand);
        gameObjectInHand.transform.localPosition = Vector3.zero;
        gameObjectInHand.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
        gameObjectInHand.transform.localScale = Vector3.one;
        CurrentItem = gameObjectInHand.GetComponent<Item>();
    }

    public void AddDamage(int damage)
    {
        Damage += damage;
        _textInterface[0].text = Damage.ToString();
        _eventDamage.Invoke();
    }

    public void ItemChanges()
    {
        _textInterface[1].text = Mushrooms.ToString();
        _eventMushroom.Invoke();
    }
}
