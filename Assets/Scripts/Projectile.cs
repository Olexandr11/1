using UnityEngine;

public class Projectile : MonoBehaviour
{
    [HideInInspector] public Player HeadPlayer;

    void Update()
    {
        transform.Translate(Vector3.right * 0.33f);
    }

    public void FlyBullet(float aTan)
    {
        float angle = 360 + aTan;
        transform.rotation = Quaternion.Euler(0f, 0f, angle);
        Destroy(gameObject, 1.5f);
    }

     private void OnTriggerEnter2D(Collider2D collision)
     {
         if (collision.gameObject.TryGetComponent(out NonPlayerCharacter monster))
         {
             monster.TakeDamage(HeadPlayer.Damage, HeadPlayer);

             Destroy(gameObject);
         }
      } 
}
