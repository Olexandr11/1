﻿using UnityEngine;
using System.Collections;

public class Pursuer : NonPlayerCharacter
{

    private Player _player;
    private Walk _walk;
    private SpriteRenderer _meSprite;
  
    private Color _color;

    private Vector3 _direction;

    private void Start()
    {
        _meSprite = gameObject.GetComponent<SpriteRenderer>();
        _color = _meSprite.color;
        
        
        _walk = FindObjectOfType<Walk>(true);
        _player = FindObjectOfType<Player>(true);
    }

    private void FixedUpdate()
    {
        MoveDirection();
        transform.Translate(_direction * Time.deltaTime);
    }

    public override void Interection(Player player)
    {
        player.Mistakes++;
        _walk.SwapOnGhost();
    }

    public override void MoveDirection()
    {
       _direction = (_player.transform.position - transform.position).normalized;
    }

    public override void TakeDamage(int damage, Player player)
    {
        StartCoroutine(Fading());
        if (transform.localScale.x < 2f) transform.localScale *= damage * 0.25f;
    }

    private IEnumerator Fading()
    {
        _meSprite.color = new Color(_meSprite.color.r, _meSprite.color.g, _meSprite.color.b, 0.2f);

        for (int i = 0; i < 8; i++)
        {
            _meSprite.color = new Color(_meSprite.color.r, _meSprite.color.g, _meSprite.color.b, _meSprite.color.a + 0.1f);
            yield return new WaitForSeconds(0.1f);
        }

    }
}

