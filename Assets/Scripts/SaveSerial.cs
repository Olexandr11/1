using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveSerial : MonoBehaviour
{
    public List<GameObject> ObjectToLoad = new List<GameObject>();

    private string _saveString = "1";

    private void Awake()
    {
        _saveString = Application.dataPath + "SaveGameFoxy.json";       
    }
    public void SaveGame()
    {
        if(!File.Exists(_saveString)) File.Create(_saveString);
        
        SaveData saveData = new SaveData();       
        saveData.SaveObjects(ObjectToLoad);
        
        string json = JsonUtility.ToJson(saveData);       
        File.WriteAllText(_saveString, json);
    }

    public void LoadGame()
    {
        if (!File.Exists(_saveString)) return;

        string jsonString = File.ReadAllText(_saveString);
        SaveData save = JsonUtility.FromJson<SaveData>(jsonString);

        for(int i = 0; ObjectToLoad.Count>i; i++)
        {
            ObjectToLoad[i].transform.position =
                new Vector3(save.ObjectsSave[i].Position.X, save.ObjectsSave[i].Position.Y, save.ObjectsSave[i].Position.Z);
        }
    }
}

[System.Serializable]
public class SaveData
{
    [System.Serializable]
    public struct Vec3
    {
        public float X, Y, Z;
        public Vec3(float x,float y,float z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
    [System.Serializable]
    public struct SaveObjectsData
    {
        public Vec3 Position,Direction;

        public SaveObjectsData(Vec3 pos, Vec3 dir)
        {
            Position = pos;
            Direction = dir;
        }
    }

    public List<SaveObjectsData> ObjectsSave = new List<SaveObjectsData>();

    public void SaveObjects(List<GameObject> game)
    {
        foreach (var GO in game)
        {
            Vec3 pos = new Vec3(GO.transform.position.x, GO.transform.position.y, GO.transform.position.z);           
            Vec3 dir = new Vec3(GO.transform.rotation.x, GO.transform.rotation.y, GO.transform.rotation.z);        
            ObjectsSave.Add(new SaveObjectsData(pos,dir));           
        }
    }


}