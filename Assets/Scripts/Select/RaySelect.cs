using UnityEngine;

public class RaySelect : MonoBehaviour
{
    public RaycastHit2D hit;

    [SerializeField] private Material _selectMaterial;

    private Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        hit = Physics2D.Raycast(_camera.ScreenToWorldPoint(Input.mousePosition), Vector3.zero);
        if (hit.collider)
        {
            if (hit.collider.TryGetComponent(out SelectingGM selectingGM)) selectingGM.Select(hit,_selectMaterial);
        }
    }
}
