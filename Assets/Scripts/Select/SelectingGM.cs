using UnityEngine;
using System.Collections;

public class SelectingGM : MonoBehaviour
{
    private SpriteRenderer _myRenderer;
    private Material _defaultMaterial;

    private void Start()
    {
        _myRenderer = GetComponent<SpriteRenderer>();
        _defaultMaterial = _myRenderer.material;
    }

    public void Select(RaycastHit2D hit, Material material)
    {
        _myRenderer.material = material;
        StartCoroutine(MaterialToDefault(hit));
    }
    private IEnumerator MaterialToDefault(RaycastHit2D hit)
    {
        while (true)
        {
            if(hit.collider.gameObject != gameObject)
            {
                _myRenderer.material = _defaultMaterial;
                yield break;               
            }
            yield return null;
        }
    }
}
