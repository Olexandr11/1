using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartManager : MonoBehaviour
{
    [SerializeField] private Text _text;
    private void Start()
    {
        _text.text = "Record: "+ PlayerPrefs.GetFloat("Record").ToString()+"s";
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0)) SceneManager.LoadScene(1);
    }  
}
