using UnityEngine;
using System.Collections;

public class VectorFire : MonoBehaviour
{
    public Joystick JoystickFire;
    [SerializeField] private GameObject _arrow;

    private bool _isShoting;
    private Player _player;

    private void Start()
    {
        _player = gameObject.GetComponent<Player>();
        _isShoting = true;
        _arrow.SetActive(!_isShoting);
    }

    private void Update()
    {
        if (JoystickFire.Direction != Vector2.zero)
        {
            WeaponCheck();
            _isShoting = false;
        }
        else 
        {
            Shot();
        }
    }

    private void Shot()
    {
        if (_isShoting) return;

        _player.Intepact(_player.CurrentItem);
        _isShoting = true;
        _arrow.SetActive(!_isShoting);
    }
    private void WeaponCheck()
    {
        if (_player.CurrentItem.Type != Item.TypeUse.Weapon) return;
        _arrow.transform.position = new Vector3(transform.position.x, transform.position.y - 0.05f, transform.position.z);
        _player.Angle = Mathf.Atan2(JoystickFire.Direction.y, JoystickFire.Direction.x) * Mathf.Rad2Deg;
        _arrow.transform.rotation = Quaternion.AngleAxis(_player.Angle, Vector3.forward);
        _arrow.SetActive(!_isShoting);
    }
}
