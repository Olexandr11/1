using UnityEngine;
using UnityEngine.EventSystems;

public class Walk : MonoBehaviour,IPointerDownHandler, IPointerUpHandler,IDragHandler
{
    [SerializeField] private Sprite _ghost;
    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _buttonSpawn;

    private float _speed = 0.05f;
    private bool _isRight;

    private SpriteRenderer _playerSprite;
    private Collider2D _playerCollider;
    private Animator animator;
    private Joystick _joystick;

    private void Start()
    {
        _playerCollider = _player.GetComponent<Collider2D>();
        _playerSprite = _player.GetComponent<SpriteRenderer>();

        _joystick = gameObject.GetComponent<Joystick>();
        
        animator = _player.GetComponent<Animator>();
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (_joystick.Horizontal > 0)
        {
            if (_isRight) return;

            _player.transform.localScale = new Vector3(Mathf.Abs(_player.transform.localScale.x), _player.transform.localScale.y, 1f);
            _isRight = true;
        }
        else
        {
            if (_isRight == false) return;

            _player.transform.localScale = new Vector3(-_player.transform.localScale.x, _player.transform.localScale.y, 1f);
            _isRight = false;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
        
        animator.SetBool("run", true);  
    }

    public void OnPointerUp(PointerEventData eventData)
    {       
        animator.SetBool("run", false);
    }
    private void FixedUpdate()
    {
        _player.transform.Translate(_joystick.transform.up * _joystick.Vertical * _speed);
        _player.transform.Translate(_joystick.transform.right * _joystick.Horizontal * _speed);
    }
    public void SwapOnGhost()
    {
        animator.enabled = false;
        
        _buttonSpawn.SetActive(true);
        
        _playerSprite.sprite = _ghost;
        _playerCollider.enabled = false;
    } 
    public void SwapOnFox()
    {
         animator.enabled = true;
        
        _buttonSpawn.SetActive(false);
        
        _playerSprite.sprite = null;
        _playerCollider.enabled = true;
        
    }
}
