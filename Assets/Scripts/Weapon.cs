using UnityEngine;

public abstract class Item : MonoBehaviour
{
    public abstract void Interection(Player player);
    public abstract TypeUse Type { get; }
    public enum TypeUse {Weapon,Baf}
}
