using UnityEngine;

[CreateAssetMenu(menuName = "Item")]
public class AssetItem : ScriptableObject, IItem
{
    public string Name => _name;
    public Sprite Icon => _icon;
    public GameObject PrefabObject => _prefab;
    public IItem.TypeItem Type => _type;

    [SerializeField] private string _name;
    [SerializeField] private Sprite _icon;
    [SerializeField] private IItem.TypeItem _type;
    [SerializeField] private GameObject _prefab;
}
