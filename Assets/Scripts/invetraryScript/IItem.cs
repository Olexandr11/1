using UnityEngine;

public interface IItem
{
    string Name { get; }
    Sprite Icon { get; }
    GameObject PrefabObject { get; }
    TypeItem Type { get; }

    enum TypeItem {Weapon,Food}
}
