using UnityEngine;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
    [SerializeField] private List<AssetItem> Items;
    [SerializeField] private InventoryCell _cellTemplate;
    [SerializeField] private Transform _container;
    
    private Transform[] _containerCells;
    private Canvas _canvas;

    private void Awake()
    {
        _canvas = GetComponentInChildren<Canvas>(true);
        _containerCells = new Transform[_container.childCount];

        for (int i = 0; i < _container.childCount; i++)
        {
            _containerCells[i] = _container.GetChild(i);
        }
    }

    private void OnEnable()
    {
        Render(Items);
    }

    public void Render(List<AssetItem> items)
    {
        int i = 0;
        items.ForEach(item => 
        {
            var cell = Instantiate(_cellTemplate, _containerCells[i]);
            cell.Render(item);
            cell.ItemCell = item;
            cell.SetCanvas(_canvas.transform);
            i++;
        });
    }
}
