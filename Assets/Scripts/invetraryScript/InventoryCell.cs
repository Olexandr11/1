using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]
public class InventoryCell : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] private Text _nameField;
    [SerializeField] private Image _iconField;

    [HideInInspector] public AssetItem ItemCell;

    private CanvasGroup _group;
    private Transform _canvas;

    private void Start()
    {
        _group = GetComponent<CanvasGroup>();        
    }   

    public void OnBeginDrag(PointerEventData eventData)
    {
        transform.SetParent(_canvas);
        _group.blocksRaycasts = false;

        OnDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _group.blocksRaycasts = true;
        StartCoroutine(CheckDrop());
    }

    public void Render(IItem item)
    {
        _nameField.text = item.Name;
        _iconField.sprite = item.Icon;
    }

    public void SetCanvas(Transform parent)
    {
        _canvas = parent;
    }

    private IEnumerator CheckDrop()
    {
        yield return new WaitForSeconds(0.1f);
        if (gameObject.transform.parent == _canvas) Destroy(gameObject);
    }

}
