using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MainslotInventory : MonoBehaviour, IDropHandler
{
    [SerializeField] private Player _player;

    private void Start()
    {
        _player.SwapIItem(transform.GetChild(0).GetComponent<InventoryCell>().ItemCell);
        StartCoroutine(CheckUp(transform.GetChild(0).GetComponent<InventoryCell>()));
    }
    public void OnDrop(PointerEventData eventData)
    {
        if(eventData.pointerDrag.TryGetComponent(out InventoryCell cell))
        {
            cell.transform.SetParent(transform);
            cell.transform.localPosition = Vector3.zero;
            _player.SwapIItem(cell.ItemCell);
            StartCoroutine(CheckUp(cell));
        }
    }
    private IEnumerator CheckUp(InventoryCell cell)
    {
        while (true)
        {
            if(cell.transform.parent != transform)
            {
                _player.SwapIItem(null);
                yield break;
            }
            yield return null;
        }
    }
}
