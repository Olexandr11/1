using UnityEngine;
using UnityEngine.EventSystems;

public class SlotInventory : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag.TryGetComponent(out InventoryCell cell))
        {
            cell.transform.SetParent(transform);
            cell.transform.localPosition = Vector3.zero;
        }
    }
}
